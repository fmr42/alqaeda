import sys
import numpy as np
import cv2

from cv2 import cv2


a = cv2.imread("a.bmp")
b = cv2.imread("b.bmp")
s=a.copy()
rows,cols,chans= np.shape(a)

for i in range(rows):
    for j in range(cols):
        for k in range(chans):
            s[i,j,k] = ( (a[i,j,k]/8.0).astype('uint16') << 3 ) + ( b[i,j,k]/32.0 ).astype('uint8')

cv2.imwrite('s.bmp',s)
cv2.imshow('a',a)
cv2.waitKey(100)
cv2.imshow('b',b)
cv2.waitKey(100)
cv2.imshow('s',s)
cv2.waitKey(0)
cv2.destroyAllWindows()



